import { callApi } from '../helpers/apiHelper';
import { Fighter, FighterDetails } from '../helpers/mockData';

class FighterService {
  public async getFighters(): Promise<Fighter[]> {
    try {
      const endpoint = 'fighters.json';
      return await callApi<Fighter[]>(endpoint, 'GET');
    } catch (error) {
      throw error;
    }
  }

  public async getFighterDetails(id: number | string): Promise<FighterDetails> {
    // todo: implement this method
    const endpoint = `details/fighter/${id}.json`;
    return await callApi<FighterDetails>(endpoint, 'GET');
  }
}

export const fighterService = new FighterService();
