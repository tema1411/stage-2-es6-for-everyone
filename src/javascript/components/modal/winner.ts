import { showModal } from './modal';
import App from '../../app';
import { FighterDetails } from '../../helpers/mockData';

export function showWinnerModal(fighter: FighterDetails) {
  const { name } = fighter;

  showModal({
    title: 'Fight finished',
    bodyElement: `<b>${name}</b> won!!!`,
    onClose: () => {
      document.getElementById('root')!.innerHTML = '';
      new App();
    },
  });
}
