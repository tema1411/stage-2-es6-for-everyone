import { createElement } from '../../helpers/domHelper';

interface FinishModal {
  title: string,
  bodyElement: string,
  onClose?: () => void
}

export function showModal({
                            title, bodyElement, onClose = () => {
  }
                          }: FinishModal) {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });

  root!.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal({ title, bodyElement, onClose }: FinishModal) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);
  const body = createBody(bodyElement);

  modalContainer.append(header, body);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose?: () => void) {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = () => {
    hideModal();
    onClose && onClose();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function createBody(bodyElement: string) {
  const body = createElement({ tagName: 'div', className: 'modal-body' });
  body.innerHTML = bodyElement;
  return body;
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
