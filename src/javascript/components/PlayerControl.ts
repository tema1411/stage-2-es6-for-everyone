import { getDamage, getCriticalDamage } from './fight';
import { FighterDetails } from '../helpers/mockData';

interface PlayerControlState {
  isAttacking: boolean,
  isImpactBlock: boolean,
  isCanUseCritical: boolean,
}

class PlayerControl {
  private health: number;
  private readonly attackingFighter: any;
  private readonly attackBtnKey: string;
  private readonly blockAttackBtnKey: string;
  private criticalAttackKeys: string[];
  private state: PlayerControlState;
  private readonly defendingFighter: FighterDetails;
  private defendingPlayerControl: PlayerControl | null;
  private pressed: Set<string>;
  private readonly getWinnerFighter: any;
  private $healthIndicator: HTMLElement | null;

  constructor({
                attackerFighter,
                defendingFighter,
                attackBtnKey,
                blockAttackBtnKey,
                criticalAttackKey,
                getWinnerFighterCallback,
                selectorHealthIndicator
              }) {
    this.health = attackerFighter.health;
    this.attackingFighter = attackerFighter;
    this.attackBtnKey = attackBtnKey;
    this.blockAttackBtnKey = blockAttackBtnKey;
    this.criticalAttackKeys = criticalAttackKey;

    this.state = {
      isAttacking: false,
      isImpactBlock: false,
      isCanUseCritical: true
    };

    this.defendingFighter = defendingFighter;
    this.defendingPlayerControl = null;

    this.pressed = new Set();
    this.getWinnerFighter = getWinnerFighterCallback;

    this._controlFighter = this._controlFighter.bind(this);
    this._resetState = this._resetState.bind(this);
    this.$healthIndicator = document.getElementById(selectorHealthIndicator);
  }

  _defaultHit() {
    const damage = getDamage(this.attackingFighter, this.defendingFighter);
    this.defendingPlayerControl?.takeDamage(damage);
  }

  _criticalHit() {
    this.state.isCanUseCritical = false;
    const intervalUseCriticalHit:number = 10000;

    const damage = getCriticalDamage(this.attackingFighter.attack);
    this.defendingPlayerControl?.takeDamage(damage, true);

    setTimeout(() => (this.state.isCanUseCritical = true), intervalUseCriticalHit);
  }

  takeDamage(damage: number, isCritical?: boolean) {
    if (isCritical || !this.state.isImpactBlock) {
      this._downHealth(damage);
    }
  }

  _checkDeath() {
    if (this.health <= 0) {
      this.getWinnerFighter(this.defendingFighter);
      this.offPlayerControl();
      this.defendingPlayerControl?.offPlayerControl();
    }
  }

  _getNewWidthHealthIndicator() {
    const maxHealth = this.attackingFighter.health;
    const currentHealth = this.health;

    const newWidthIndicator = (currentHealth / maxHealth) * 100;
    return newWidthIndicator < 0 ? 0 : newWidthIndicator;
  }

  _updateHealthIndicator() {
    const newWidthIndicator = this._getNewWidthHealthIndicator();
    this.$healthIndicator!.style.width = `${newWidthIndicator}%`;
  }

  _downHealth(damage: number) {
    this.health -= damage;
    this._updateHealthIndicator();
    this._checkDeath();
  }

  _isCriticalCombination() {
    return this.criticalAttackKeys.every((btnCode) => this.pressed.has(btnCode));
  }

  _resetState({ code }: {code: string}) {
    if (code === this.attackBtnKey) {
      this.state.isAttacking = false;
    }


    if (code === this.blockAttackBtnKey) {
      this.state.isImpactBlock = false;
    }

    this.pressed.delete(code);
  }

  _controlFighter({ code }: {code: string}) {
    if (code === this.blockAttackBtnKey) {
      this.state.isImpactBlock = true;
    }

    if (code === this.attackBtnKey && !this.state.isAttacking && !this.state.isImpactBlock) {
      this.state.isAttacking = true;
      this._defaultHit();
    }

    this.pressed.add(code);

    if (this._isCriticalCombination() && this.state.isCanUseCritical && !this.state.isImpactBlock) {
      this._criticalHit();
    }
  }

  _onPlayerControl() {
    window.addEventListener('keydown', this._controlFighter);
    window.addEventListener('keyup', this._resetState);
  }

  offPlayerControl() {
    window.removeEventListener('keydown', this._controlFighter);
    window.removeEventListener('keyup', this._resetState);
  }

  init(defendingPlayerControl: PlayerControl) {
    this.defendingPlayerControl = defendingPlayerControl;
    this._onPlayerControl();
  }
}

export default PlayerControl;
