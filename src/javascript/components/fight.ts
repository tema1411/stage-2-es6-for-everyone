import { controls } from '../../constants/controls';
import { getRandomNumber } from '../helpers/mathHelper';
import PlayerControl from './PlayerControl';
import { FighterDetails } from '../helpers/mockData';

export async function fight(firstFighter: FighterDetails, secondFighter: FighterDetails): Promise<FighterDetails> {
  return new Promise((resolve) => {
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerTwoCriticalHitCombination
    } = controls;

    const controlFirstPlayer = new PlayerControl({
      attackerFighter: firstFighter,
      defendingFighter: secondFighter,
      attackBtnKey: PlayerOneAttack,
      blockAttackBtnKey: PlayerOneBlock,
      criticalAttackKey: PlayerOneCriticalHitCombination,
      getWinnerFighterCallback: resolve,
      selectorHealthIndicator: 'left-fighter-indicator'
    });

    const controlSecondPlayer = new PlayerControl({
      attackerFighter: secondFighter,
      defendingFighter: firstFighter,
      attackBtnKey: PlayerTwoAttack,
      blockAttackBtnKey: PlayerTwoBlock,
      criticalAttackKey: PlayerTwoCriticalHitCombination,
      getWinnerFighterCallback: resolve,
      selectorHealthIndicator: 'right-fighter-indicator'
    });

    controlFirstPlayer.init(controlSecondPlayer);
    controlSecondPlayer.init(controlFirstPlayer);
  });
}

export function getCriticalDamage(attack: number) {
  const gain = 2;
  return gain * attack;
}

export function getDamage(attacker: FighterDetails, defender: FighterDetails) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0 , damage);
}

export function getHitPower(fighter: FighterDetails) {
  const criticalHitChance = getRandomNumber(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: FighterDetails) {
  const dodgeChance = getRandomNumber(1, 2);
  return fighter.defense * dodgeChance;
}
