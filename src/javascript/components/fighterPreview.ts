import { createElement } from '../helpers/domHelper';
import { FighterDetails } from '../helpers/mockData';

export function createFighterPreview(fighter: FighterDetails | undefined, position: 'right' | 'left') {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  if (fighter) {
    const { source, ...mainFighterInfo } = fighter;
    fighterElement.append(createFighterImage(fighter));
    fighterElement.append(createInfoFighterRows(mainFighterInfo as FighterDetails));
  }

  return fighterElement;
}

function createInfoTableData(nameParam: string) {
  const rowInfo = createElement({
    tagName: 'td',
    className: 'fighter-preview___data'
  });

  rowInfo.textContent = nameParam;
  return rowInfo;
}

function createInfoFighterRow(nameParam: string, valueParam: string) {
  const rowInfo = createElement({
    tagName: 'tr',
    className: 'fighter-preview___row'
  });

  rowInfo.append(createInfoTableData(nameParam));
  rowInfo.append(createInfoTableData(valueParam));

  return rowInfo;
}

function createInfoFighterRows(fighter: FighterDetails) {
  const listParamWrp = createElement({
    tagName: 'table',
    className: 'fighter-preview___rows'
  });
  const { _id, ...mainFighterInfo } = fighter;
  Object.keys(mainFighterInfo).forEach((nameParam) => listParamWrp.append(createInfoFighterRow(nameParam, fighter[nameParam])));

  return listParamWrp;
}

export function createFighterImage(fighter: FighterDetails) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };

  return createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

}
